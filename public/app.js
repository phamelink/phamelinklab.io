window.onscroll = function() {scrollFunction()};
window.onload = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50 || screen.width <= 500) {
    document.getElementById("header").style.height = "8vh";
    document.getElementById("header-logo-text-h1").style.fontSize = "2.2vh";
  } else {
    document.getElementById("header").style.height = "15vh";
    document.getElementById("header-logo-text-h1").style.fontSize = "4vh";
  }
}

const burger = document.getElementById("burger")
const headerNav = document.getElementsByClassName("header-nav")[0]

function openSideMenu() {
  burger.classList.toggle("open")
  headerNav.classList.toggle("open")
  if (burger.classList.contains("open")) {
    document.body.style.overflowY = "hidden";
  }
}


